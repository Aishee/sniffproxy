#!/usr/bin/env python
# -*- coding: utf-8 -*-

class Producer():
    """
    Generating class IP
    """
    def ip2num(self, i):
        ip =[int(x) for x in i.split('.')]
        return ip[0] << 24 | ip[1] << 16 | ip[2] << 8 | ip[3]
    def num2ip(self, n):
        return '%s.%s.%s.%s' % (
            (n & 0xff000000) >> 24,
            (n & 0x00ff0000) >> 16,
            (n & 0x0000ff00) >> 8,
            n & 0x000000ff
        )
    def ports(self):
        return [80, 81, 8080, 9999]
    
    def gen(self, s, e):
        return [self.num2ip(n) for n in range(self.ip2num(s), self.ip2num(e)+1) if n & 0xff]
    
    def chunks(self, arr, n):
        return [arr[i:i+n] for i in range(0, len(arr), n)]
    