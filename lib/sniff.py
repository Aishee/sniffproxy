#!/usr/bin/env python
# -*- coding: utf-8 -*-

import requests
from producer import Producer
import gevent.monkey
from multiprocessing import Process

gevent.monkey.patch_socket()

pd = Producer()
CHECK_LENGTH = 2695

class Sniff():
    def __init__(self, startIP, endIP, processNum, threadNum):
        self.startIP = startIP
        self.endIP = endIP
        self.processNum = processNum
        self.threadNum = threadNum
    
    def valid_proxy(self, proxy_url):
        try:
            proxys = {"http": proxy_url}
            r = requests.get(
                "",
                proxys = proxys,
                timeout=4
            )
            is_valid = False
            if len(r.text) == CHECK_LENGTH:
                is_valid = True
            return is_valid
        except requests.exceptions.ProxyError:
            return False
        except Exception:
            return False
        
    def valid_proxy_job(self, queue):
        while len(queue) > 0:
            host = queue.pop()
            for port in pd.ports():
                proxy = "http://%s:%s" % (host, port)
                is_valid = self.valid_proxy(proxy)
                if is_valid:
                    print "good %s" % proxy
    
    def valid_proxys_with_gevent(self, proxy_list):
        threads = []
        for i in range(int(self.threadNum)):
            threads.append(gevent.spawn(self.valid_proxy_job, proxy_list))
        gevent.joinall(threads)
    
    def valid_proxy_with_multiprocess(self):
        proxy_list = pd.gen(self.startIP, self.endIP)
        step = len(proxy_list) / self.processNum
        chunk_proxy_list = pd.chunks(proxy_list, step)
        
        process = []
        for item in chunk_proxy_list:
            p = Process(target=self.valid_proxys_with_gevent, args=(item, ))
            p.start()
            process.append(p)
        for item in process:
            p.join()