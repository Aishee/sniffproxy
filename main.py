#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
from lib.sniff import Sniff

def get_opt():
    display = '''Running'''
    print display
    parser = argparse.ArgumentParser()
    parser.add_argument('--start_ip', type=str, nargs='?')
    parser.add_argument('--end_ip', type=str, nargs='?', default="")
    parser.add_argument('--process_num', type=int, nargs='?', default="")
    parser.add_argument('--thread_num', type=int, nargs='?', default="")
    args = parser.parse_args()
    return args

if __name__ == '__main__':
    args = get_opt()
    instance = Sniff(args.start_ip, args.end_ip, args.process_num, args.thread_num)
    instance.valid_proxy_with_multiprocess()